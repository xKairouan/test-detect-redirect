// -*- coding: utf-8 -*-
// ref: http://ido.nu/kuma/2009/10/13/chromium-extension-hacks-1-detecting-xmlhttprequest-redirection/

'use strict';

(function () {
    var loaded = 0;
    var url = document.querySelector('a[rel="next"]').href;
    var req = new XMLHttpRequest();
    req.addEventListener('progress', function (e) {
        loaded = e.loaded;
    }, false);
    req.addEventListener('readystatechange', function (e) {
        if (e.target.readyState === 4) {
            var length = e.target.getResponseHeader('Content-Length');
            length = parseInt(length, 10);
            if (length !== loaded) {
                console.log('detected!');
            }
            else {
                console.log('not detected...', length, loaded);
            }
        }
    }, false);
    req.open('GET', url, true);
    req.send(null);
}());

/*
 * Environment:
 *   UA: Mozilla/5.0 (X11; Linux i686; rv:11.0a2) Gecko/20120131 Firefox/11.0a2
 *   Add-on SDK: 1.5b3
 *
 * Result (in Web Console):
 *   not detected... 1208 1208
 * Result (in Add-on):
 *   not detected... 1208 1208
 */
